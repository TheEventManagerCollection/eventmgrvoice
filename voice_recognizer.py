import speech_recognition as sr     # import the library
import os
from gtts import gTTS
from playsound import playsound

from EventMgrAPI.User import User
 
r = sr.Recognizer()                 # initialize recognizer
def do_tts(text):
	tts = gTTS(text)
	tts.save('/tmp/text.mp3')
	playsound('/tmp/text.mp3')
	
def interaction():
	user_id = 0
	description = ""
	
	do_tts("Heya everyone! I'm a bot starting as of today! Please be nice to me and I'll work as hard as I can! Smiley! Now, what is your user ID? Remember, you should just say your ID. From CuteyBot!")
	#do_tts("Hi. Say your ID.")
	
	with sr.Microphone() as source: # mention source it will be either Microphone or audio files.
		audio = r.listen(source)        # listen to the source
		user_id = r.recognize_google(audio)
		
	user = User(user_id)
	
	print(user_id)
		
	do_tts(f"Hey {user.first_name}, and what are you requesting?")    # recognize speech using Google Speech Recognition
	
	with sr.Microphone() as source:
		audio_two = r.listen(source)        # listen to the source
		description = r.recognize_google(audio_two)
		
	
	print(description)		
	do_tts(f"Your request was {description}. Ok, thank you so much. Hugs and kisses!")    # recognize speech using Google Speech Recognition
	user.create_rs_request(description)
	
	
	
		
